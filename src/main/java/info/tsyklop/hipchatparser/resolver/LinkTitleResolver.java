package info.tsyklop.hipchatparser.resolver;

import java.util.Optional;

public interface LinkTitleResolver {
    Optional<String> resolve(String url);
}
