package info.tsyklop.hipchatparser.resolver;

import info.tsyklop.hipchatparser.model.Link;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
public class LinkTitleResolverImpl implements LinkTitleResolver {

    @Override
    public Optional<String> resolve(String url) {

        try {
            return Optional.ofNullable(Jsoup.connect(url).get().title());
        } catch (IOException ignored) { }

        return Optional.empty();

    }

}
