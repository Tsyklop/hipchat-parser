package info.tsyklop.hipchatparser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParseResult {

    private List<Link> links;

    private List<String> mentions;

    private List<String> emoticons;

    public void addLink(Link link) {
        Objects.requireNonNull(link, "Link cannot be null");
        if(Objects.isNull(links)) {
            this.links = new ArrayList<>();
        }
        this.links.add(link);
    }

    public void addMention(String mention) {
        Objects.requireNonNull(mention, "Mention cannot be null");
        if(Objects.isNull(mentions)) {
            this.mentions = new ArrayList<>();
        }
        this.mentions.add(mention);
    }

    public void addEmoticon(String emoticon) {
        Objects.requireNonNull(emoticon, "Emoticon cannot be null");
        if(Objects.isNull(emoticons)) {
            this.emoticons = new ArrayList<>();
        }
        this.emoticons.add(emoticon);
    }

}
