package info.tsyklop.hipchatparser.controller;

import info.tsyklop.hipchatparser.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/chat", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ChatRestController {

    private final ChatService chatService;

    @RequestMapping("/parse")
    public ResponseEntity parse(@RequestBody String message) {
        return this.chatService.parse(message);
    }

}
