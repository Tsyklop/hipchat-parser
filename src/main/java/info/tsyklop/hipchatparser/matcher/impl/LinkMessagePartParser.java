package info.tsyklop.hipchatparser.matcher.impl;

import info.tsyklop.hipchatparser.matcher.MessagePartParser;
import info.tsyklop.hipchatparser.model.Link;
import info.tsyklop.hipchatparser.model.ParseResult;
import info.tsyklop.hipchatparser.resolver.LinkTitleResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class LinkMessagePartParser implements MessagePartParser {

    private final LinkTitleResolver linkTitleResolver;

    private static final Pattern PATTERN = Pattern.compile("(https?://[^\\s]+[\\w]?\\b)");

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    @Override
    public void parse(ParseResult parseResult, String message) {

        Matcher matcher = getPattern().matcher(message);

        while (matcher.find()) {

            String url = matcher.group(1);

            Optional<String> title = this.linkTitleResolver.resolve(url);

            parseResult.addLink(
                    Link.builder()
                            .url(url)
                            .title(title.orElse("Unknown"))
                            .build()
            );

        }

    }

}
