package info.tsyklop.hipchatparser.matcher.impl;

import info.tsyklop.hipchatparser.matcher.MessagePartParser;
import info.tsyklop.hipchatparser.model.ParseResult;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class EmotionsMessagePartParser implements MessagePartParser {

    private static final Pattern PATTERN = Pattern.compile("\\((\\w+)\\)");

    @Override
    public Pattern getPattern() {
        return PATTERN;
    }

    @Override
    public void parse(ParseResult parseResult, String message) {

        Matcher matcher = getPattern().matcher(message);

        while (matcher.find()) {
            parseResult.addEmoticon(matcher.group(1));
        }

    }

}
