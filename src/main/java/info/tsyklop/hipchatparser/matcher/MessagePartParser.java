package info.tsyklop.hipchatparser.matcher;

import info.tsyklop.hipchatparser.model.ParseResult;

import java.util.regex.Pattern;

public interface MessagePartParser {

    Pattern getPattern();

    void parse(ParseResult parseResult, String message);

}
