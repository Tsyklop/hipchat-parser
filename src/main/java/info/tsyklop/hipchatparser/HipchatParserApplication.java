package info.tsyklop.hipchatparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HipchatParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(HipchatParserApplication.class, args);
    }

}
