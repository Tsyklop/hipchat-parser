package info.tsyklop.hipchatparser.service.impl;

import info.tsyklop.hipchatparser.matcher.MessagePartParser;
import info.tsyklop.hipchatparser.model.ParseResult;
import info.tsyklop.hipchatparser.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final List<MessagePartParser> messagePartParsers;

    @Deprecated
    private static final Pattern MESSAGE_PATTERN = Pattern.compile("@(\\w+\\b)|\\((\\w+)\\)|(https?://[^\\s]+[\\w]?\\b)");

    @Override
    public ResponseEntity parse(String message) {

        ParseResult parseResult = new ParseResult();

        for (MessagePartParser messagePartParser : this.messagePartParsers) {
            messagePartParser.parse(parseResult, message);
        }

        /*Matcher matcher = MESSAGE_PATTERN.matcher(message);

        while (matcher.find()) {

            if (Objects.nonNull(matcher.group(1))) {
                parseResult.addMention(matcher.group(1));
            } else if (Objects.nonNull(matcher.group(2))) {
                parseResult.addEmoticon(matcher.group(2));
            } else if (Objects.nonNull(matcher.group(3))) {

                String url = matcher.group(3);

                Optional<String> title = this.linkTitleResolver.resolve(url);

                parseResult.addLink(
                        Link.builder()
                                .url(url)
                                .title(title.orElse("Unknown"))
                                .build()
                );

            }

        }*/

        return ResponseEntity.ok(parseResult);

    }

}
