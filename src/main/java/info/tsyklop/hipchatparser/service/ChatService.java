package info.tsyklop.hipchatparser.service;

import org.springframework.http.ResponseEntity;

public interface ChatService {
    ResponseEntity parse(String message);
}
